from django.apps import AppConfig


class ConversionapiConfig(AppConfig):
    name = 'conversionAPI'
