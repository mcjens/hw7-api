from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.utils import timezone
from django.urls import reverse
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from random import randint
from conversionAPI.models import Convert
import json


def getit(request):
    # print(request)
    valueGetter = request.GET.get('value')
    fromGetter = request.GET.get('from')
    # print(fromGetter)
    toGetter = request.GET.get('to')
    retreiveFrom = Convert.objects.get(unit=fromGetter)
    doConvert = retreiveFrom.value * float(valueGetter)
    # print(doConvert)

    data = {}
    data['unit'] = toGetter
    data['value'] = doConvert
    jsonDict = json.dumps(data)
    # print(jsonDict)
    return HttpResponse(jsonDict)

def init(request):
    Convert.objects.all().delete()
    this = Convert(unit='lbs', value='14.5833')
    this.save()
    return HttpResponse('DB has some stuff')
