from django.urls import path

from . import views

urlpatterns = [
    path('json', views.getit, name='getit'),
    path('', views.init, name="init"),
]
