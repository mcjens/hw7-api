from django.urls import path

from . import views

urlpatterns = [
    path('converter', views.getit, name='getit'),
    path('', views.init, name="init")

]
