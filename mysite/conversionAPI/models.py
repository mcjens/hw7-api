from django.db import models

class Convert(models.Model):
    unit = models.CharField(max_length=200)
    value = models.FloatField()

def __str__(self):
    return '%s, %f' % (self.unit, self.value)
