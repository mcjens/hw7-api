from django.apps import AppConfig


class GoldformConfig(AppConfig):
    name = 'goldform'
