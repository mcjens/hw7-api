from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('conversionAPI/', include('conversionAPI.urls')),
    path('init/', include('conversionAPI.urls')),
    path('goldform/', include('goldform.urls')),
    path('', include('goldform.urls')),
    path('admin/', admin.site.urls),
]
